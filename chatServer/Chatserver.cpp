#include "Chatserver.h"

#include <iostream>

#include "QtWebSockets/QWebSocketServer"
#include "QtWebSockets/QWebSocket"
#include <QtCore/QDebug>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

QT_USE_NAMESPACE

ChatServer::ChatServer(quint16 port, QObject *parent)
:  QObject(parent),
   server(Q_NULLPTR),
   clients(),
   clientsList()
{
   qDebug() << "Chat Server listening on port";
   server = new QWebSocketServer(QStringLiteral("Chat Server"),
                                 QWebSocketServer::NonSecureMode,
                                 this);

   if(server->listen(QHostAddress::Any, port))
   {
      qDebug() << "Chat Server listening on port" << port;
      connect(server,
              &QWebSocketServer::newConnection,
              this,
              &ChatServer::onNewConnection);
   }
}

void ChatServer::onNewConnection()
{
   QWebSocket* clientSocket = server->nextPendingConnection();

   connect(clientSocket, SIGNAL(binaryFrameReceived(QByteArray, bool)), this, SLOT(binaryFrame(QByteArray, bool)));
   connect(clientSocket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));

   clients << clientSocket;
}

void ChatServer::binaryFrame(const QByteArray &frame, bool isLastFrame)
{
   if(isLastFrame)
   {
      QWebSocket* socket = qobject_cast<QWebSocket*>(sender());
      if(socket == 0)
      {
         return;
      }

      const QJsonDocument doc = QJsonDocument::fromBinaryData(frame);
      const QJsonObject jsonMessage = doc.object();

      const QJsonValue messageType = jsonMessage.value("message_type");

      if((int)MessageType::message == messageType.toInt())
      {
         QWebSocket* client;
         foreach(client, clients)
         {
            client->sendBinaryMessage(frame);
         }
      }
      else if((int)MessageType::registerUser == messageType.toInt())
      {
         const QJsonValue user = jsonMessage.value("name");
         clientsList << user.toString();
         updateUsersList();
      }
      else if((int)MessageType::unregisterUser == messageType.toInt())
      {
         const QJsonValue user = jsonMessage.value("name");
         clientsList.removeOne(user.toString());
         updateUsersList();
      }
   }
}

void ChatServer::socketDisconnected()
{
   QWebSocket* socket = qobject_cast<QWebSocket*>(sender());
   if (socket == 0)
   {
      return;
   }
   clients.removeOne(socket);
   socket->deleteLater();
}

void ChatServer::updateUsersList()
{
    QJsonArray users;
    QString name;
    foreach(name, clientsList)
    {
       users.append(QJsonValue(name));
    }

    QJsonObject jsonMessage;
    jsonMessage.insert("message_type", QJsonValue((int)MessageType::usersNameList));
    jsonMessage.insert("usersName", QJsonValue(users));

    const QJsonDocument doc(jsonMessage);
    const QByteArray data = doc.toBinaryData();

    QWebSocket* client;
    foreach(client, clients)
    {
       client->sendBinaryMessage(data);
    }
}

ChatServer::~ChatServer()
{
   server->close();
   qDeleteAll(clients.begin(), clients.end());
}
