#ifndef CHATSERVER_H
#define CHATSERVER_H

#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QByteArray>

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)

enum class MessageType
{
   registerUser = 0,
   unregisterUser = 1,
   usersNameList = 2,
   message = 3
};

class ChatServer : public QObject
{
   Q_OBJECT

public:
   explicit ChatServer(quint16 port, QObject *parent = Q_NULLPTR);
   virtual ~ChatServer();

private Q_SLOTS:
   void onNewConnection();
   void binaryFrame(const QByteArray &frame, bool isLastFrame);
   void socketDisconnected();

private:
   void updateUsersList();

   QWebSocketServer *server;
   QList<QWebSocket *> clients;
   QList<QString> clientsList;
};

#endif // CHATSERVER_H
