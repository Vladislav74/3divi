QT += core websockets
QT -= gui

CONFIG += c++11

TARGET = chatServer
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    Chatserver.cpp

HEADERS += \
    Chatserver.h


