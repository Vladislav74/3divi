#include <QCoreApplication>
#include "Chatserver.h"

int main(int argc, char *argv[])
{
   QCoreApplication a(argc, argv);
   ChatServer server(8080);

   return a.exec();
}
