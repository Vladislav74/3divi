#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QDialog>

namespace Ui {
class Loginwindow;
}

class Loginwindow : public QDialog
{
   Q_OBJECT

public:
   explicit Loginwindow(QWidget *parent = 0);
   ~Loginwindow();

private:
   Ui::Loginwindow *ui;

private slots:
   void chatButtonEnabled(const QString message);
   void chatButtonWasPushed();

};

#endif // LOGINWINDOW_H
