#include "Chatclientengine.h"

#include "Usersessioninfo.h"

ChatClientEngine::ChatClientEngine(QObject *parent) : QObject(parent)
{
    QString url = QString("%1:%2").arg(UserSessionInfo::Instance()->getServerIpAddress()).arg(UserSessionInfo::Instance()->getServerPort());
    wsSocket.open(QUrl(url));

    //connect(&wsSocket, &QWebSocket::textMessageReceived, this, &ChatClientEngine::displayMessage);
}

void ChatClientEngine::sendMessage()
{
    QString pseudo = UserSessionInfo::Instance()->getUserName();

   // QString message = ui->textInputField->text();

    //wsSocket->sendTextMessage(QString("%1: %2").arg(pseudo).arg(message));
}

void ChatClientEngine::displayMessage(QString message)
{
    qDebug() << "Message received:" << message;
    //ui->chatView->append(message);

}

void ChatClientEngine::displaySslErrors(const QList<QSslError>& errors)
{
    for (int i=0, sz=errors.size(); i<sz; i++)
    {
        QString errorString = errors.at(i).errorString();
        displayMessage(errorString);
    }
}

void ChatClientEngine::disconnectSocket()
{
    //
}

void ChatClientEngine::socketConnected()
{
    displayMessage(tr("CONNECTED"));
}

void ChatClientEngine::socketDisconnected()
{
    displayMessage(tr("DISCONNECTED"));
}

void ChatClientEngine::socketStateChanged(QAbstractSocket::SocketState socketState)
{
    /*
    switch (socketState)
    {
        case QAbstractSocket::UnconnectedState:
            ui->socketStateLabel->setText(tr("Unconnected"));
            break;
        case QAbstractSocket::HostLookupState:
            ui->socketStateLabel->setText(tr("HostLookup"));
            break;
        case QAbstractSocket::ConnectingState:
            ui->socketStateLabel->setText(tr("Connecting"));
            break;
        case QAbstractSocket::ConnectedState:
            ui->socketStateLabel->setText("Connected");
            break;
        case QAbstractSocket::BoundState:
            ui->socketStateLabel->setText(tr("Bound"));
            break;
        case QAbstractSocket::ClosingState:
            ui->socketStateLabel->setText(tr("Closing"));
            break;
        case QAbstractSocket::ListeningState:
            ui->socketStateLabel->setText(tr("Listening"));
            break;
        default:
            ui->socketStateLabel->setText(tr("Unknown"));
            break;
    }
    */
}
