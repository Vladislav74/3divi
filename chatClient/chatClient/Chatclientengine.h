#ifndef CHATCLIENTENGINE_H
#define CHATCLIENTENGINE_H

#include <QDialog>
#include "QtWebSockets/QWebSocket"
#include <QObject>

class ChatClientEngine : public QObject
{
    Q_OBJECT
public:
    explicit ChatClientEngine(QObject *parent = 0);

private:
    QWebSocket wsSocket;

signals:

private slots:
    void socketConnected();
    void socketDisconnected();
    void sendMessage();
    void connectSocket();
    void disconnectSocket();
    void displayMessage(QString message);
    void socketStateChanged(QAbstractSocket::SocketState socketState);
    void displaySslErrors(const QList<QSslError>& errors);

};

#endif // CHATCLIENTENGINE_H
