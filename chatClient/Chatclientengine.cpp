#include "Chatclientengine.h"

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

#define SERVER_ADDRESS "ws://localhost:8080"

ChatClientEngine::ChatClientEngine(QObject *parent)
: QObject(parent),
  listOfUsers()
{
   user = "user";

   connect(&wsSocket, SIGNAL(binaryFrameReceived(QByteArray, bool)), this, SLOT(binaryMessageFrameReceived(QByteArray, bool)));
   connect(&wsSocket, SIGNAL(connected()), this, SLOT(socketConnected()));
   connect(&wsSocket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
}

void ChatClientEngine::sendMessage(const QString user, const QString message)
{
    QJsonObject jsonMessage;
    jsonMessage.insert("message_type", QJsonValue((int)MessageType::message));
    jsonMessage.insert("name", QJsonValue(user));
    jsonMessage.insert("message", QJsonValue(message));

    const QJsonDocument doc(jsonMessage);
    const QByteArray data = doc.toBinaryData();

    wsSocket.sendBinaryMessage(data);
}

void ChatClientEngine::tryToConnect(const QString newUser)
{
   user = newUser;
   wsSocket.open(QUrl(SERVER_ADDRESS));
}

void ChatClientEngine::tryToDisconect()
{
    QJsonObject jsonMessage;
    jsonMessage.insert("message_type", QJsonValue((int)MessageType::unregisterUser));
    jsonMessage.insert("name", QJsonValue(user));

    const QJsonDocument doc(jsonMessage);
    const QByteArray data = doc.toBinaryData();

    wsSocket.sendBinaryMessage(data);
    wsSocket.close();
}

void ChatClientEngine::binaryMessageFrameReceived(const QByteArray &frame, bool isLastFrame)
{
   if(isLastFrame)
   {
      const QJsonDocument doc = QJsonDocument::fromBinaryData(frame);
      const QJsonObject jsonMessage = doc.object();

      const QJsonValue messageType = jsonMessage.value("message_type");

      if((int)MessageType::message == messageType.toInt())
      {
         const QJsonValue user = jsonMessage.value("name");
         const QJsonValue newMessage = jsonMessage.value("message");

         emit newMessageWasReceived(user.toString(), newMessage.toString());
      }
      else if((int)MessageType::usersNameList == messageType.toInt())
      {
         const QJsonValue users = jsonMessage.value("usersName");
         QJsonArray usersName = users.toArray();

         listOfUsers.clear();
         QJsonValue user;
         foreach(user, usersName)
         {
            listOfUsers << user.toString();
         }
         emit updateUsersList(listOfUsers);
      }
   }
}

void ChatClientEngine::disconnectSocket()
{
   wsSocket.close();
}

void ChatClientEngine::socketConnected()
{
   QJsonObject jsonMessage;
   jsonMessage.insert("message_type", QJsonValue((int)MessageType::registerUser));
   jsonMessage.insert("name", QJsonValue(user));

   const QJsonDocument doc(jsonMessage);
   const QByteArray data = doc.toBinaryData();

   wsSocket.sendBinaryMessage(data);

   emit connectStateWasUpdated(true);
}

void ChatClientEngine::socketDisconnected()
{
   emit connectStateWasUpdated(false);
}


