#-------------------------------------------------
#
# Project created by QtCreator 2016-08-14T21:17:54
#
#-------------------------------------------------

QT += core gui websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = chatClient
TEMPLATE = app

SOURCES += main.cpp \
    Chatwindow.cpp \
    Chatclientengine.cpp \
    Loginwindow.cpp

HEADERS  += \
    Chatwindow.h \
    Chatclientengine.h \
    Loginwindow.h

FORMS    += \
    Chatwindow.ui \
    Loginwindow.ui

target.path = $$/Users/vladislav/3DiVi/chatClient/chatClient
INSTALLS += target



