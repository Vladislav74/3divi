#include "Chatwindow.h"

#include "ui_chatwindow.h"

#define CONNECTED "Connected"
#define DISCONNECTED "Disconnected"
#define CONNECT "Connect"
#define DISCONNECT "Disconnect"

Chatwindow::Chatwindow(QWidget *parent)
:  QDialog(parent),
   ui(new Ui::Chatwindow)
{
   userName = "User";
   bConnectState = false;
   ui->setupUi(this);

   ui->userNameLabel->setText(userName);

   ui->sendButton->setEnabled(false);

   connect(ui->textInputField, SIGNAL(textChanged(QString)), this, SLOT(sendButtonEnabled(QString)));
   connect(ui->sendButton, SIGNAL(clicked()), this, SLOT(sendButtonWasPushed()));
   connect(ui->connectAndDisconnectButton, SIGNAL(clicked()), this, SLOT(connectAndDisconnectButtonWasPushed()));

   connect(&chatClientEngine,SIGNAL(newMessageWasReceived(QString, QString)), this, SLOT(newMessageWasReceived(QString, QString)));
   connect(&chatClientEngine,SIGNAL(updateUsersList(QList<QString>)), this, SLOT(updateUsersList(QList<QString>)));
   connect(&chatClientEngine,SIGNAL(connectStateWasUpdated(bool)), this, SLOT(connectStateWasUpdated(bool)));
}

void Chatwindow::setUserName(const QString name)
{
   if(!(name.isEmpty()))
   {
      userName = name;
      ui->userNameLabel->setText(userName);
   }
}

const QString Chatwindow::getUserName()
{
   return userName;
}

void Chatwindow::newMessageWasReceived(const QString user, const QString message)
{
   const QString chatMessage = QString("%1:%2").arg(user).arg(message);
   ui->messagesView->append(chatMessage);
}

void Chatwindow::updateUsersList(const QList<QString> listOfUsers)
{
   ui->listOfUsersView->clear();

   QString user;
   foreach(user, listOfUsers)
   {
      ui->listOfUsersView->append(user);
   }
}

void Chatwindow::connectStateWasUpdated(const bool bConnected)
{
   if(bConnected)
   {
      ui->ConnectStateLabel->setText(CONNECTED);
      ui->connectAndDisconnectButton->setText(DISCONNECT);
      bConnectState = true;
   }
   else
   {
      ui->ConnectStateLabel->setText(DISCONNECTED);
      ui->connectAndDisconnectButton->setText(CONNECT);
      bConnectState = false;
   }
}

void Chatwindow::sendButtonEnabled(const QString message)
{
   if(!(message.isEmpty()))
   {
       ui->sendButton->setEnabled(true);
   }
}

void Chatwindow::sendButtonWasPushed()
{
  ui->sendButton->setEnabled(false);

  const QString message = ui->textInputField->text();
  const QString user = getUserName();
  ui->textInputField->clear();

  chatClientEngine.sendMessage(user, message);
}

void Chatwindow::connectAndDisconnectButtonWasPushed()
{
   if(!bConnectState)
   {
      chatClientEngine.tryToConnect(getUserName());
   }
   else
   {
      ui->listOfUsersView->clear();
      chatClientEngine.tryToDisconect();
   }
}

Chatwindow::~Chatwindow()
{
   delete ui;
}


