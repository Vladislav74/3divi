#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include "Chatclientengine.h"
#include <QDialog>

namespace Ui {
class Chatwindow;
}

class Chatwindow : public QDialog
{
   Q_OBJECT

public:
   explicit Chatwindow(QWidget *parent = 0);
   ~Chatwindow();

   void setUserName(const QString name);
   const QString getUserName();

private:
   Ui::Chatwindow *ui;
   ChatClientEngine chatClientEngine;

   QString userName;
   bool bConnectState;

private slots:
   void newMessageWasReceived(const QString user, const QString message);
   void updateUsersList(const QList<QString> listOfUsers);
   void connectStateWasUpdated(const bool bConnected);

   void sendButtonEnabled(const QString message);
   void sendButtonWasPushed();
   void connectAndDisconnectButtonWasPushed();
};

#endif // CHATWINDOW_H
