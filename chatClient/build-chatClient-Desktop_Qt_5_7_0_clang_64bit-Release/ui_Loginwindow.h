/********************************************************************************
** Form generated from reading UI file 'Loginwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWINDOW_H
#define UI_LOGINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Loginwindow
{
public:
    QWidget *widget;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_2;
    QLineEdit *ipAddress;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *loginTextField;
    QPushButton *chatButton;
    QLabel *errorLabel;

    void setupUi(QDialog *Loginwindow)
    {
        if (Loginwindow->objectName().isEmpty())
            Loginwindow->setObjectName(QStringLiteral("Loginwindow"));
        Loginwindow->resize(400, 300);
        widget = new QWidget(Loginwindow);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(120, 40, 146, 169));
        verticalLayout_4 = new QVBoxLayout(widget);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_2 = new QLabel(widget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_3->addWidget(label_2);

        ipAddress = new QLineEdit(widget);
        ipAddress->setObjectName(QStringLiteral("ipAddress"));

        verticalLayout_3->addWidget(ipAddress);


        verticalLayout_4->addLayout(verticalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(widget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        loginTextField = new QLineEdit(widget);
        loginTextField->setObjectName(QStringLiteral("loginTextField"));

        verticalLayout->addWidget(loginTextField);

        chatButton = new QPushButton(widget);
        chatButton->setObjectName(QStringLiteral("chatButton"));

        verticalLayout->addWidget(chatButton);


        verticalLayout_2->addLayout(verticalLayout);

        errorLabel = new QLabel(widget);
        errorLabel->setObjectName(QStringLiteral("errorLabel"));

        verticalLayout_2->addWidget(errorLabel);


        verticalLayout_4->addLayout(verticalLayout_2);

        chatButton->raise();
        label->raise();
        loginTextField->raise();
        label->raise();
        errorLabel->raise();
        ipAddress->raise();
        label_2->raise();

        retranslateUi(Loginwindow);

        QMetaObject::connectSlotsByName(Loginwindow);
    } // setupUi

    void retranslateUi(QDialog *Loginwindow)
    {
        Loginwindow->setWindowTitle(QApplication::translate("Loginwindow", "loginWindow", 0));
        label_2->setText(QApplication::translate("Loginwindow", "ip \320\260\320\264\321\200\320\265\321\201 \321\201\320\265\321\200\320\262\320\265\321\200\320\260 ", 0));
        label->setText(QApplication::translate("Loginwindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\201\320\262\320\276\320\271 \320\273\320\276\320\263\320\270\320\275:", 0));
        chatButton->setText(QApplication::translate("Loginwindow", "\320\222\320\276\320\271\321\202\320\270 \320\262 \321\207\320\260\321\202", 0));
        errorLabel->setText(QApplication::translate("Loginwindow", "\320\233\320\276\320\263\320\270\320\275 \320\267\320\260\320\275\321\217\321\202", 0));
    } // retranslateUi

};

namespace Ui {
    class Loginwindow: public Ui_Loginwindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWINDOW_H
