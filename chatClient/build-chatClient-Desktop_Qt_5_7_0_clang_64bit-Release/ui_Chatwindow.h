/********************************************************************************
** Form generated from reading UI file 'Chatwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHATWINDOW_H
#define UI_CHATWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Chatwindow
{
public:
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QListView *chatView;
    QTextBrowser *usersView;
    QHBoxLayout *horizontalLayout;
    QLineEdit *textInputField;
    QPushButton *sendButton;

    void setupUi(QDialog *Chatwindow)
    {
        if (Chatwindow->objectName().isEmpty())
            Chatwindow->setObjectName(QStringLiteral("Chatwindow"));
        Chatwindow->resize(558, 300);
        widget = new QWidget(Chatwindow);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(20, 10, 526, 239));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        chatView = new QListView(widget);
        chatView->setObjectName(QStringLiteral("chatView"));

        horizontalLayout_2->addWidget(chatView);

        usersView = new QTextBrowser(widget);
        usersView->setObjectName(QStringLiteral("usersView"));

        horizontalLayout_2->addWidget(usersView);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        textInputField = new QLineEdit(widget);
        textInputField->setObjectName(QStringLiteral("textInputField"));

        horizontalLayout->addWidget(textInputField);

        sendButton = new QPushButton(widget);
        sendButton->setObjectName(QStringLiteral("sendButton"));

        horizontalLayout->addWidget(sendButton);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(Chatwindow);

        QMetaObject::connectSlotsByName(Chatwindow);
    } // setupUi

    void retranslateUi(QDialog *Chatwindow)
    {
        Chatwindow->setWindowTitle(QApplication::translate("Chatwindow", "Dialog", 0));
        sendButton->setText(QApplication::translate("Chatwindow", "\320\236\321\202\320\277\321\200\320\260\320\262\320\270\321\202\321\214", 0));
    } // retranslateUi

};

namespace Ui {
    class Chatwindow: public Ui_Chatwindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHATWINDOW_H
