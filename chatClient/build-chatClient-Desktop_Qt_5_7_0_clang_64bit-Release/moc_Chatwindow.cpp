/****************************************************************************
** Meta object code from reading C++ file 'Chatwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../chatClient/Chatwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Chatwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Chatwindow_t {
    QByteArrayData data[17];
    char stringdata0[252];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Chatwindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Chatwindow_t qt_meta_stringdata_Chatwindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "Chatwindow"
QT_MOC_LITERAL(1, 11, 17), // "sendButtonEnabled"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 7), // "message"
QT_MOC_LITERAL(4, 38, 19), // "sendButtonWasPushed"
QT_MOC_LITERAL(5, 58, 15), // "socketConnected"
QT_MOC_LITERAL(6, 74, 18), // "socketDisconnected"
QT_MOC_LITERAL(7, 93, 11), // "sendMessage"
QT_MOC_LITERAL(8, 105, 13), // "connectSocket"
QT_MOC_LITERAL(9, 119, 16), // "disconnectSocket"
QT_MOC_LITERAL(10, 136, 14), // "displayMessage"
QT_MOC_LITERAL(11, 151, 18), // "socketStateChanged"
QT_MOC_LITERAL(12, 170, 28), // "QAbstractSocket::SocketState"
QT_MOC_LITERAL(13, 199, 11), // "socketState"
QT_MOC_LITERAL(14, 211, 16), // "displaySslErrors"
QT_MOC_LITERAL(15, 228, 16), // "QList<QSslError>"
QT_MOC_LITERAL(16, 245, 6) // "errors"

    },
    "Chatwindow\0sendButtonEnabled\0\0message\0"
    "sendButtonWasPushed\0socketConnected\0"
    "socketDisconnected\0sendMessage\0"
    "connectSocket\0disconnectSocket\0"
    "displayMessage\0socketStateChanged\0"
    "QAbstractSocket::SocketState\0socketState\0"
    "displaySslErrors\0QList<QSslError>\0"
    "errors"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Chatwindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   64,    2, 0x08 /* Private */,
       4,    0,   67,    2, 0x08 /* Private */,
       5,    0,   68,    2, 0x08 /* Private */,
       6,    0,   69,    2, 0x08 /* Private */,
       7,    0,   70,    2, 0x08 /* Private */,
       8,    0,   71,    2, 0x08 /* Private */,
       9,    0,   72,    2, 0x08 /* Private */,
      10,    1,   73,    2, 0x08 /* Private */,
      11,    1,   76,    2, 0x08 /* Private */,
      14,    1,   79,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, 0x80000000 | 12,   13,
    QMetaType::Void, 0x80000000 | 15,   16,

       0        // eod
};

void Chatwindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Chatwindow *_t = static_cast<Chatwindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendButtonEnabled((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->sendButtonWasPushed(); break;
        case 2: _t->socketConnected(); break;
        case 3: _t->socketDisconnected(); break;
        case 4: _t->sendMessage(); break;
        case 5: _t->connectSocket(); break;
        case 6: _t->disconnectSocket(); break;
        case 7: _t->displayMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->socketStateChanged((*reinterpret_cast< QAbstractSocket::SocketState(*)>(_a[1]))); break;
        case 9: _t->displaySslErrors((*reinterpret_cast< const QList<QSslError>(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractSocket::SocketState >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QSslError> >(); break;
            }
            break;
        }
    }
}

const QMetaObject Chatwindow::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Chatwindow.data,
      qt_meta_data_Chatwindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Chatwindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Chatwindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Chatwindow.stringdata0))
        return static_cast<void*>(const_cast< Chatwindow*>(this));
    return QDialog::qt_metacast(_clname);
}

int Chatwindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
