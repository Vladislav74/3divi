#include "Loginwindow.h"

#include "ui_Loginwindow.h"
#include "Chatwindow.h"

Loginwindow::Loginwindow(QWidget *parent)
:  QDialog(parent),
   ui(new Ui::Loginwindow)
{
   ui->setupUi(this);

   QRegExp exp("[a-zA-Z]{1,15}[0-9]{0,10}");
   ui->userNameField->setValidator(new QRegExpValidator(exp, this));

   ui->chatButton->setEnabled(false);

   connect(ui->userNameField, SIGNAL(textChanged(QString)), this, SLOT(chatButtonEnabled(QString)));
   connect(ui->chatButton, SIGNAL(clicked()), this, SLOT(chatButtonWasPushed()));
}

void Loginwindow::chatButtonEnabled(const QString message)
{
   if(!(message.isEmpty()))
   {
      ui->chatButton->setEnabled(true);
   }
   else
   {
      ui->chatButton->setEnabled(false);
   }
}

void Loginwindow::chatButtonWasPushed()
{
   this->hide();
   Chatwindow *chatWindow = new Chatwindow();
   chatWindow->setUserName(ui->userNameField->text());
   chatWindow->show();
}

Loginwindow::~Loginwindow()
{
    delete ui;
}
