#ifndef CHATCLIENTENGINE_H
#define CHATCLIENTENGINE_H

#include <QObject>
#include "QtWebSockets/QWebSocket"

enum class MessageType
{
   registerUser = 0,
   unregisterUser = 1,
   usersNameList = 2,
   message = 3
};

class ChatClientEngine : public QObject
{
   Q_OBJECT

public:
   explicit ChatClientEngine(QObject *parent = 0);

   void sendMessage(const QString user, const QString message);
   void tryToConnect(const QString newUser);
   void tryToDisconect();

private:
   QWebSocket wsSocket;
   QString user;

   QList<QString> listOfUsers;

signals:
   void newMessageWasReceived(const QString user, const QString message);
   void updateUsersList(const QList<QString> listOfUsers);
   void connectStateWasUpdated(const bool bConnected);

private slots:
   void binaryMessageFrameReceived(const QByteArray &frame, bool isLastFrame);
   void socketConnected();
   void socketDisconnected();
   void disconnectSocket();
};

#endif // CHATCLIENTENGINE_H
