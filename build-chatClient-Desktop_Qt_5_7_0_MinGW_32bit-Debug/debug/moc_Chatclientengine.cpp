/****************************************************************************
** Meta object code from reading C++ file 'Chatclientengine.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../chatClient/Chatclientengine.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Chatclientengine.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_ChatClientEngine_t {
    QByteArrayData data[16];
    char stringdata0[227];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ChatClientEngine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ChatClientEngine_t qt_meta_stringdata_ChatClientEngine = {
    {
QT_MOC_LITERAL(0, 0, 16), // "ChatClientEngine"
QT_MOC_LITERAL(1, 17, 21), // "newMessageWasReceived"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 4), // "user"
QT_MOC_LITERAL(4, 45, 7), // "message"
QT_MOC_LITERAL(5, 53, 15), // "updateUsersList"
QT_MOC_LITERAL(6, 69, 14), // "QList<QString>"
QT_MOC_LITERAL(7, 84, 11), // "listOfUsers"
QT_MOC_LITERAL(8, 96, 22), // "connectStateWasUpdated"
QT_MOC_LITERAL(9, 119, 10), // "bConnected"
QT_MOC_LITERAL(10, 130, 26), // "binaryMessageFrameReceived"
QT_MOC_LITERAL(11, 157, 5), // "frame"
QT_MOC_LITERAL(12, 163, 11), // "isLastFrame"
QT_MOC_LITERAL(13, 175, 15), // "socketConnected"
QT_MOC_LITERAL(14, 191, 18), // "socketDisconnected"
QT_MOC_LITERAL(15, 210, 16) // "disconnectSocket"

    },
    "ChatClientEngine\0newMessageWasReceived\0"
    "\0user\0message\0updateUsersList\0"
    "QList<QString>\0listOfUsers\0"
    "connectStateWasUpdated\0bConnected\0"
    "binaryMessageFrameReceived\0frame\0"
    "isLastFrame\0socketConnected\0"
    "socketDisconnected\0disconnectSocket"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ChatClientEngine[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   49,    2, 0x06 /* Public */,
       5,    1,   54,    2, 0x06 /* Public */,
       8,    1,   57,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    2,   60,    2, 0x08 /* Private */,
      13,    0,   65,    2, 0x08 /* Private */,
      14,    0,   66,    2, 0x08 /* Private */,
      15,    0,   67,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Bool,    9,

 // slots: parameters
    QMetaType::Void, QMetaType::QByteArray, QMetaType::Bool,   11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ChatClientEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ChatClientEngine *_t = static_cast<ChatClientEngine *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newMessageWasReceived((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 1: _t->updateUsersList((*reinterpret_cast< const QList<QString>(*)>(_a[1]))); break;
        case 2: _t->connectStateWasUpdated((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 3: _t->binaryMessageFrameReceived((*reinterpret_cast< const QByteArray(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 4: _t->socketConnected(); break;
        case 5: _t->socketDisconnected(); break;
        case 6: _t->disconnectSocket(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QString> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (ChatClientEngine::*_t)(const QString , const QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ChatClientEngine::newMessageWasReceived)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (ChatClientEngine::*_t)(const QList<QString> );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ChatClientEngine::updateUsersList)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (ChatClientEngine::*_t)(const bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&ChatClientEngine::connectStateWasUpdated)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject ChatClientEngine::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ChatClientEngine.data,
      qt_meta_data_ChatClientEngine,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *ChatClientEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ChatClientEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_ChatClientEngine.stringdata0))
        return static_cast<void*>(const_cast< ChatClientEngine*>(this));
    return QObject::qt_metacast(_clname);
}

int ChatClientEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void ChatClientEngine::newMessageWasReceived(const QString _t1, const QString _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ChatClientEngine::updateUsersList(const QList<QString> _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ChatClientEngine::connectStateWasUpdated(const bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
