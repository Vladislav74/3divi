/********************************************************************************
** Form generated from reading UI file 'Chatwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHATWINDOW_H
#define UI_CHATWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Chatwindow
{
public:
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *infoUserNameLabel;
    QLabel *userNameLabel;
    QSpacerItem *horizontalSpacer;
    QLabel *infoListOfUsersLabel;
    QHBoxLayout *horizontalLayout_4;
    QTextBrowser *messagesView;
    QTextEdit *listOfUsersView;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *textInputField;
    QPushButton *sendButton;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *connectAndDisconnectButton;
    QLabel *ConnectInfoLabel;
    QLabel *ConnectStateLabel;
    QSpacerItem *horizontalSpacer_4;

    void setupUi(QDialog *Chatwindow)
    {
        if (Chatwindow->objectName().isEmpty())
            Chatwindow->setObjectName(QStringLiteral("Chatwindow"));
        Chatwindow->resize(600, 345);
        Chatwindow->setMinimumSize(QSize(600, 345));
        widget = new QWidget(Chatwindow);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(40, 10, 526, 313));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        infoUserNameLabel = new QLabel(widget);
        infoUserNameLabel->setObjectName(QStringLiteral("infoUserNameLabel"));

        horizontalLayout_3->addWidget(infoUserNameLabel);

        userNameLabel = new QLabel(widget);
        userNameLabel->setObjectName(QStringLiteral("userNameLabel"));

        horizontalLayout_3->addWidget(userNameLabel);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        infoListOfUsersLabel = new QLabel(widget);
        infoListOfUsersLabel->setObjectName(QStringLiteral("infoListOfUsersLabel"));

        horizontalLayout_3->addWidget(infoListOfUsersLabel);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        messagesView = new QTextBrowser(widget);
        messagesView->setObjectName(QStringLiteral("messagesView"));

        horizontalLayout_4->addWidget(messagesView);

        listOfUsersView = new QTextEdit(widget);
        listOfUsersView->setObjectName(QStringLiteral("listOfUsersView"));

        horizontalLayout_4->addWidget(listOfUsersView);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        textInputField = new QLineEdit(widget);
        textInputField->setObjectName(QStringLiteral("textInputField"));

        horizontalLayout_2->addWidget(textInputField);

        sendButton = new QPushButton(widget);
        sendButton->setObjectName(QStringLiteral("sendButton"));

        horizontalLayout_2->addWidget(sendButton);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_3 = new QSpacerItem(13, 17, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        connectAndDisconnectButton = new QPushButton(widget);
        connectAndDisconnectButton->setObjectName(QStringLiteral("connectAndDisconnectButton"));

        horizontalLayout->addWidget(connectAndDisconnectButton);

        ConnectInfoLabel = new QLabel(widget);
        ConnectInfoLabel->setObjectName(QStringLiteral("ConnectInfoLabel"));

        horizontalLayout->addWidget(ConnectInfoLabel);

        ConnectStateLabel = new QLabel(widget);
        ConnectStateLabel->setObjectName(QStringLiteral("ConnectStateLabel"));

        horizontalLayout->addWidget(ConnectStateLabel);

        horizontalSpacer_4 = new QSpacerItem(13, 17, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(Chatwindow);

        QMetaObject::connectSlotsByName(Chatwindow);
    } // setupUi

    void retranslateUi(QDialog *Chatwindow)
    {
        Chatwindow->setWindowTitle(QApplication::translate("Chatwindow", "Dialog", 0));
        infoUserNameLabel->setText(QApplication::translate("Chatwindow", "\320\222\320\260\321\210\320\265 \320\270\320\274\321\217:", 0));
        userNameLabel->setText(QApplication::translate("Chatwindow", "User", 0));
        infoListOfUsersLabel->setText(QApplication::translate("Chatwindow", "\320\241\320\277\320\270\321\201\320\276\320\272 \320\277\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\320\265\320\271:", 0));
        sendButton->setText(QApplication::translate("Chatwindow", "\320\236\321\202\320\277\321\200\320\260\320\262\320\270\321\202\321\214", 0));
        connectAndDisconnectButton->setText(QApplication::translate("Chatwindow", "Connect", 0));
        ConnectInfoLabel->setText(QApplication::translate("Chatwindow", "\320\241\320\276\321\201\321\202\320\276\321\217\320\275\320\270\320\265 \320\241\320\276\320\265\320\264\320\265\320\275\320\270\320\275\320\270\321\217 ", 0));
        ConnectStateLabel->setText(QApplication::translate("Chatwindow", "Disconnected", 0));
    } // retranslateUi

};

namespace Ui {
    class Chatwindow: public Ui_Chatwindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHATWINDOW_H
