/********************************************************************************
** Form generated from reading UI file 'Loginwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWINDOW_H
#define UI_LOGINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Loginwindow
{
public:
    QWidget *widget;
    QGridLayout *gridLayout;
    QLabel *infoLabel;
    QLineEdit *userNameField;
    QPushButton *chatButton;

    void setupUi(QDialog *Loginwindow)
    {
        if (Loginwindow->objectName().isEmpty())
            Loginwindow->setObjectName(QStringLiteral("Loginwindow"));
        Loginwindow->resize(233, 126);
        Loginwindow->setMinimumSize(QSize(233, 126));
        widget = new QWidget(Loginwindow);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(20, 20, 193, 82));
        gridLayout = new QGridLayout(widget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        infoLabel = new QLabel(widget);
        infoLabel->setObjectName(QStringLiteral("infoLabel"));

        gridLayout->addWidget(infoLabel, 0, 0, 1, 1);

        userNameField = new QLineEdit(widget);
        userNameField->setObjectName(QStringLiteral("userNameField"));

        gridLayout->addWidget(userNameField, 1, 0, 1, 1);

        chatButton = new QPushButton(widget);
        chatButton->setObjectName(QStringLiteral("chatButton"));

        gridLayout->addWidget(chatButton, 2, 0, 1, 1);


        retranslateUi(Loginwindow);

        QMetaObject::connectSlotsByName(Loginwindow);
    } // setupUi

    void retranslateUi(QDialog *Loginwindow)
    {
        Loginwindow->setWindowTitle(QApplication::translate("Loginwindow", "Dialog", 0));
        infoLabel->setText(QApplication::translate("Loginwindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\270\320\274\321\217 \320\264\320\273\321\217 \320\262\321\205\320\276\320\264\320\260 \320\262 \321\207\320\260\321\202", 0));
        chatButton->setText(QApplication::translate("Loginwindow", "\320\222\320\276\320\271\321\202\320\270 \320\262 \321\207\320\260\321\202", 0));
    } // retranslateUi

};

namespace Ui {
    class Loginwindow: public Ui_Loginwindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWINDOW_H
